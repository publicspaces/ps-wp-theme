<?php

$context = Timber::context();

$header_image_position = get_field('header_image_position');

$event_tags = get_field('tags');



if (!$event_tags)$event_tags = array();
$event_related_posts = array();
$event_related_posts = Timber::get_posts(array(
    'tag_slug__in' => array_map(function($t){return $t->slug;},$event_tags),
    'posts_per_page' => 3,
    'post__not_in' => array($post->ID),
    'post_status' => 'publish',
    'orderby' => array( 'date' => 'DESC' )
));

$event_location = get_field('location');
$event_cta = get_field('cta');
$event_date_start = get_field('date_start');
$event_date_end = get_field('date_end');

$context['header_image_position'] = $header_image_position;
$context['event_tags'] = $event_tags;
$context['event_related_posts'] = $event_related_posts;

$context['event_location'] = $event_location;
$context['buttons'] = $event_cta['link'] ? array(array(
        'presentation' => 'primary',
        'link' => $event_cta['link']
    )) : array();
$context['event_date_start'] = $event_date_start;
$context['event_date_end'] = $event_date_end;




Timber::render('event.twig', $context);
