<?php

$columns = get_field('columns');

$context = Timber::context();
$context['columns'] = $columns;

Timber::render("blocks/grid.twig", $context);
