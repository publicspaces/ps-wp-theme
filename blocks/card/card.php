<?php

$title = get_field('title');
$description = get_field('description');
$image = get_field('image');
$link = get_field('link');

$context = Timber::context();
$context['title'] = $title;
$context['description'] = $description;
$context['image'] = $image;
$context['link'] = $link;

Timber::render("blocks/card.twig", $context);
