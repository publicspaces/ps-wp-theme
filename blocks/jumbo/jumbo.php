<?php

$title = get_field('title');
$description = get_field('description');
$image = get_field('image');
$image_position = get_field('image_position');
$buttons = get_field('buttons');
$theme = get_field('theme');

$context = Timber::context();
$context['title'] = $title;
$context['description'] = $description;
$context['image'] = $image;
$context['image_position'] = $image_position;
$context['buttons'] = $buttons;
$context['theme'] = $theme;

Timber::render("blocks/jumbo.twig", $context);
