<?php

$post = Timber::get_post();

/**
 * try to get the post id from the post object so we can use it to exclude the current post from the query
 * if we can't get the post id from the post object, try to get it from the $_GET array
 * this works for the first render in the backend, but breaks when the post is updated after a change
 * TODO: find a better way to get the post id
 */
$id = $post ? $post->ID : $_GET["post"];

/**
 * TODO:
 * - add a custom view for "avatars"
 */

$title = get_field("title");
$description = get_field("description");
$list_appearance = get_field("list_appearance");
$card_options = get_field("card_options");
$post_types = get_field("post_types");
$max_posts = get_field("max_posts");
$include_categories = get_field("include_categories");
$exclude_categories = get_field("exclude_categories");
$include_tags = get_field("include_tags");
$exclude_tags = get_field("exclude_tags");
$include_orgtype = get_field("include_orgtype");

$columns = 1;
if ($list_appearance == "horizontal") {
    $columns = 1;
} elseif ($list_appearance == "vertical") {
    $columns = 2;
} elseif ($list_appearance == "card") {
    $columns = 3;
}

$context = Timber::context();
$context["title"] = $title;
$context["description"] = $description;
$context["list_appearance"] = $list_appearance;
$context["card_options"] = $card_options;
$context["post_types"] = $post_types;
$context["max_posts"] = $max_posts;
$context["include_categories"] = $include_categories;
$context["exclude_categories"] = $exclude_categories;
$context["include_tags"] = $include_tags;
$context["exclude_tags"] = $exclude_tags;

/**
 * TODO:
 * - sorting (by date, title, ascending, descending)
 * - include/exclude specific posts
 */

$date_last_week = date("Y-m-d H:i:s", strtotime("-7 days"));

$query = [
    "post_type" => $post_types,
    "post__not_in" => [$id],
    "category__in" => $include_categories,
    "category__not_in" => $exclude_categories,
    "tag__in" => $include_tags,
    "tag__not_in" => $exclude_tags,
    "posts_per_page" => $max_posts,
    "post_status" => "publish",
];

if ($post_types && in_array("event", $post_types)) {
    $query["meta_query"] = [
        "relation" => "OR", // Combine conditions with OR logic
        [
            "key" => "date_start",
            "compare" => ">=",
            "value" => $date_last_week,
            "type" => "DATETIME",
        ],
        [
            "relation" => "AND", // Combine conditions for date_end logic
            [
                "key" => "date_end",
                "compare" => "EXISTS", // Ensure date_end exists
            ],
            [
                "key" => "date_end",
                "compare" => ">=",
                "value" => $date_last_week,
                "type" => "DATETIME",
            ],
        ],
    ];
    $query["order"] = "ASC";
    $query["orderby"] = "meta_value";
    $query["meta_key"] = "date_start";
    $query["meta_type"] = "DATETIME";
}

if ($post_types && in_array("org", $post_types) && $include_orgtype) {
    $query["tax_query"] = [
        [
            "taxonomy" => "orgtype",
            "field" => "term_id",
            "terms" => $include_orgtype,
        ],
    ];
    $columns = 4;
}

$context["posts"] = Timber::get_posts($query);

$public_query = [];
if ($post_types) {
    $public_query["p"] = implode(",", $post_types);
}
if ($include_categories) {
    $public_query["c"] = implode(",", $include_categories);
}
if ($exclude_categories) {
    $public_query["cx"] = implode(",", $exclude_categories);
}
if ($include_tags) {
    $public_query["t"] = implode(",", $include_tags);
}
if ($exclude_tags) {
    $public_query["tx"] = implode(",", $exclude_tags);
}

$context["query"] = http_build_query($public_query);
$context["columns"] = $columns;

Timber::render("blocks/query.twig", $context);
