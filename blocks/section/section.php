<?php

$heading = get_field('heading');

$context = Timber::context();
$context['heading'] = $heading;

Timber::render("blocks/section.twig", $context);
