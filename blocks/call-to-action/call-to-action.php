<?php

$title = get_field('title');
$description = get_field('description');
$image = get_field('image');
$button_position = get_field('button_position');
$button_color = get_field('button_color');
$link = get_field('link');
$theme = get_field('theme');

$context = Timber::context();
$context['title'] = $title;
$context['description'] = $description;
$context['image'] = $image;
$context['button_position'] = $button_position;
$context['button_color'] = $button_color;
$context['link'] = $link;
$context['theme'] = $theme;

Timber::render("blocks/call-to-action.twig", $context);
