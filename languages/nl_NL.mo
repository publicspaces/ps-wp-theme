��    &      L  5   |      P  �   Q                    *     ?  -   Q          �  
   �     �     �     �     �     �     �     �     �                          $     9  
   ?     J     Z     i     n     s  @   �     �  8   �            +        ?  �  D  �   �     �     �     �     �     �  1   �     %	     +	     ;	     K	     Q	     c	     h	     m	     �	     �	     �	     �	  	   �	  
   �	     �	     �	     �	     �	     �	     
     !
     &
     +
  Q   7
     �
  (   �
     �
     �
  )   �
     �
                  &                                                 
                                $                       	   "                        %       !   #                   %1$s is a registered non-profit organization. Your donation is tax deductible to the extent allowable by law. No goods or service were provided by %2$s in return for this donation. Author Comments Continue reading %s Copied to clipboard. Copy to clipboard Curious about this organisation's tech stack? Date Donation Receipt Event meta F j, Y Last modified Meta More Oops... page not found Or, visit our homepage Organisations Page Pages: Projects Publication Related Related publications Share Share page Share this page Show Powerwash Team Time Translations Use this URL to share this page via email, chat or social media. Visit website Want to know more about what this organisation is about? When Where or 404 as some of you may call  this error! till Project-Id-Version: 
PO-Revision-Date: 2024-08-23 17:02+0200
Last-Translator: 
Language-Team: 
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.4.4
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __
X-Poedit-Mapping: *.twig=twig
X-Poedit-SearchPath-0: template-parts
X-Poedit-SearchPath-1: templates
X-Poedit-SearchPath-2: woocommerce
 %1$s is een non-profit organisatie met een AMBI status. Uw donatiw is mogelijk aftrekbaar van de belasting. Er zijn geen goederen of diensten geleverd door %2$s als tegenprestatie voor deze donatie. Auteur Reacties Lees %s verder Gekopieerd naar klembord. Kopieer naar klembord Benieuwd naar de tech stack van deze organisatie? Datum Besteloverzicht Praktische info j F Y Laatste wijziging Meta Meer Oeps… pagina niet gevonden Of bezoek onze homepage Organisaties Pagina Pagina’s: Projecten Publicatie Gerelateerd Gerelateerd Deel Delen Deel deze pagina Bekijk de Spoelkeuken Team Tijd Vertalingen Gebruik deze URL om de pagina te delen via e-mail, in berichten of sociale media. Bezoek de website Wil je meer weten over deze organisatie? Wanneer Waar of 404, zoals sommigen deze error noemen! t/m 