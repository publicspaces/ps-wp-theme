<?php
  	if ( has_post_thumbnail() ) {
 	    $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
    } else {
    	$featured_image = false;
    }
?>
<section class="jumbo <?php if (!$featured_image) echo 'jumbo--image-none';?>">
    <div class="text">
        <?php the_title( sprintf( '<h1 class="title">', esc_url( get_permalink() ) ), '</h1>' ); ?>
        <div class="excerpt">
            <p class="small">Description</p>
            <div class="cta">
                <a class="button button--inverted" href="#">Primary button</a>
                <a href="#">Secundary button</a>
            </div>
        </div>
    </div>
    	<?php if ( $featured_image ) : ?>
          <figure>
    	  <?php echo '<img src="'.$featured_image[0].'" width="300" height="200" alt="" />'; ?>
    	  <figcaption><?php the_post_thumbnail_caption() ?></figcaption>
    	  </figure>
        <?php endif; ?>
    
</section>
<section class="section">
<article id="post-<?php the_ID(); ?>" <?php post_class( 'article' ); ?>>
<!-- 
	<header class="entry-header mb-4">
		<?php //the_title( sprintf( '<h2 class="entry-title text-2xl md:text-3xl font-extrabold leading-tight mb-1"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
		<time datetime="<?php echo get_the_date( 'c' ); ?>" itemprop="datePublished" class="text-sm text-gray-700"><?php echo get_the_date(); ?></time>
	</header> -->

	<?php if ( is_search() || is_archive() ) : ?>

		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div>

	<?php else : ?>

		<!-- <div class="entry-content"> -->
			<?php
			/* translators: %s: Name of current post */
			the_content(
				sprintf(
					__( 'Continue reading %s', 'tailpress' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				)
			);

			wp_link_pages(
				array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'tailpress' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
					'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'tailpress' ) . ' </span>%',
					'separator'   => '<span class="screen-reader-text">, </span>',
				)
			);
			?>
		<!-- </div> -->

	<?php endif; ?>

</article>
</section>
