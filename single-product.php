<?php

$context = Timber::context();

$header_image_position = get_field('header_image_position');

$sidebar_title = get_field('sidebar_title');
$sidebar_show_post_date = get_field('sidebar_show_post_date');
$sidebar_show_post_author = get_field('sidebar_show_post_author');

$sidebar_show_related_posts = get_field('sidebar_show_related_posts');
$sidebar_related_posts = array();

$post_tags = get_the_tags();
if (!$post_tags)$post_tags = array();

$sidebar_related_posts = Timber::get_posts(array(
    'tag_slug__in' => array_map(function($t){return $t->slug;},$post_tags),
    'order_by_most_tags' => 1,
    'posts_per_page' => 5,
    'post__not_in' => array($post->ID),
    'post_status' => 'publish',
    'orderby' => array( 'date' => 'DESC' )
));


$sidebar_extras = get_field('sidebar_extras');

$context['header_image_position'] = $header_image_position;
$context['sidebar_title'] = $sidebar_title;
$context['sidebar_show_post_date'] = $sidebar_show_post_date;
$context['sidebar_show_post_author'] = $sidebar_show_post_author;
$context['sidebar_show_related_posts'] = $sidebar_show_related_posts;
$context['sidebar_related_posts'] = $sidebar_related_posts;
$context['sidebar_extras'] = $sidebar_extras;


Timber::render('product.twig', $context);
