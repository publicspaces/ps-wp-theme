<?php

/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array('archive.twig', 'index.twig');

$context = Timber::context();

$context['title'] = 'Archive';
if (is_day()) {
	$context['title'] = '' . get_the_date('D M Y');
} elseif (is_month()) {
	$context['title'] = '' . get_the_date('M Y');
} elseif (is_year()) {
	$context['title'] = '' . get_the_date('Y');
} elseif (is_tag()) {
	$context['title'] = '' . single_tag_title('', false);
} elseif (is_category()) {
	$context['title'] = '' . single_cat_title('', false);
}

global $wp_query;
$context['posts'] = new Timber\PostQuery($wp_query);

Timber::render($templates, $context);
