window.addEventListener('load', function () {

      /**
      * close submenu when clicking outside of it
      */
     
      document.addEventListener('click', function (event) {
            if (event.target.closest('.header') && (event.target.tagName === 'INPUT' || event.target.tagName === 'LABEL')) {
                  return;
            } else {
                  var checkboxes = document.querySelectorAll('.header input[type="checkbox"]');
                  for (var i = 0; i < checkboxes.length; i++) {
                        checkboxes[i].checked = false;
                  }
            }
      });
});