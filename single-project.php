<?php

$context = Timber::context();

$header_image_position = get_field('header_image_position');


$project_links = get_field('links');
$project_tags = get_field('project_tags');


$project_related_posts = array();
if ($project_tags) $project_related_posts = Timber::get_posts(array(
    'tag_slug__in' => array_map(function($t){return $t->slug;},$project_tags),
    'posts_per_page' => 3,
    'post__not_in' => array($post->ID),
    'post_status' => 'publish',
    'orderby' => array( 'date' => 'DESC' )
));

//var_dump($project_tags);
$public_query = array();
if ($project_tags && count($project_tags)>0) $public_query['t'] =implode(',',array_map(function($t){return $t->term_id;},$project_tags));
$public_query['p'] = 'post';
$context['query'] = http_build_query($public_query);


$project_contact = get_field('contact');
$project_people = get_field('people');

if (!$project_people)$project_people = array();

$context['header_image_position'] = $header_image_position;
$context['project_links'] = $project_links;
$context['project_tags'] = $project_tags;
$context['project_related_posts'] = $project_related_posts;
$context['project_contact'] = $project_contact;
$context['project_people'] = $project_people;



Timber::render('project.twig', $context);
