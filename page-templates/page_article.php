<?php /* Template Name: Acticle Content Page */ ?>
<?php

$context = Timber::context();


$header_image_position = get_field('header_image_position');


$sidebar_show_post_date = get_field('sidebar_show_post_date');
$sidebar_show_post_author = get_field('sidebar_show_post_author');
$sidebar_hide_share = get_field('sidebar_hide_share');




$sidebar_extras = get_field('sidebar_extras');

$context['header_image_position'] = $header_image_position;
$context['sidebar_show_post_date'] = $sidebar_show_post_date;
$context['sidebar_show_post_author'] = $sidebar_show_post_author;
$context['sidebar_hide_share'] = $sidebar_hide_share;

$context['sidebar_extras'] = $sidebar_extras;


Timber::render('single.twig', $context);
