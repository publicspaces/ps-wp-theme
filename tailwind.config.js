const theme = require("./theme.json");
const tailpress = require("@jeffreyvr/tailwindcss-tailpress");

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./*.php",
    "./**/*.php",
    "./*.twig",
    "./**/*.twig",
    "./resources/css/*.css",
    "./resources/js/*.js",
    "./safelist.txt",
  ],
  theme: {
    container: {
      padding: {
        DEFAULT: "1rem",
        sm: "2rem",
        lg: "0rem",
      },
    },
    extend: {
      colors: tailpress.colorMapper(
        tailpress.theme("settings.color.palette", theme),
      ),
      fontSize: tailpress.fontSizeMapper(
        tailpress.theme("settings.typography.fontSizes", theme),
      ),

      /**
       * config from PublicSpaces Web Assets repo
       * so we can add our custom components to the editor-style.css
       * TODO: preferbly load this from NPM so we keep track of versions
       */
      fontFamily: {
        body: ["UncutSans", "ui-sans-serif", "system-ui"],
        heading: ["Geomanist", "ui-sans-serif", "system-ui"],
        mono: ["MartianMono", "ui-monospace", "system-ui"],
      },
      fontSize: {
        xs: ".5rem",
        sm: ".8rem",
        base: "1rem",
        lg: "1.5rem",
        xl: "2rem",
        xxl: "4rem",
      },
      lineHeight: {
        xs: "1.4em",
        sm: "1.2em",
        base: "1.4em",
        lg: "1.2em",
        xl: "1.2em",
        xxl: "1em",
      },
      headerHeight: {
        base: "5rem",
      },
    },
    screens: {
      xs: "480px",
      sm: "600px",
      md: "782px",
      lg: tailpress.theme("settings.layout.contentSize", theme),
      xl: tailpress.theme("settings.layout.wideSize", theme),
      "2xl": "1440px",
    },
  },
  plugins: [tailpress.tailwind],
  safelist: ["aspect-video", "aspect-square"],
};
