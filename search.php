<?php

/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = ["archive.twig", "index.twig"];

$context = Timber::context();

$context["title"] = "More";

global $wp_query;
//$context['posts'] = new Timber\PostQuery($wp_query);
//var_dump($wp_query);

$search_exclusions = [];
$search_exclusions_settings = get_field("search_exclusions", "option");
if ($search_exclusions_settings) {
    $search_exclusions = array_map(function ($item) {
        return $item["page"];
    }, $search_exclusions_settings);
}

$query = [
    "post_status" => "publish",
    "s" => isset($_GET["s"]) ? $_GET["s"] : null,
    "post_type" => isset($_GET["p"])
        ? array_values(
            array_intersect(explode(",", $_GET["p"]), [
                "post",
                "organisation",
                "event",
                "page",
                "project",
            ])
        )
        : null,
    "post__not_in" => $search_exclusions,
    "tag__in" => isset($_GET["t"]) ? explode(",", $_GET["t"]) : null,
    "tag__not_in" => isset($_GET["tx"]) ? explode(",", $_GET["tx"]) : null,
    "category__in" => isset($_GET["c"]) ? explode(",", $_GET["c"]) : null,
    "category__not_in" => isset($_GET["cx"]) ? explode(",", $_GET["cx"]) : null,
    "paged" => get_query_var("paged"),
];

if (isset($_GET["o"])) {
    $query["meta_query"] = [
        [
            "key" => "organisation", // The custom field key
            "value" => '"' . $_GET["o"] . '"', // The organisation ID
            "compare" => "LIKE", // Ensures exact match
        ],
    ];
}

$context["posts"] = new Timber\PostQuery(new WP_query($query));

Timber::render($templates, $context);
