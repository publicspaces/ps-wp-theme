<?php

$context = Timber::context();

$header_image_position = get_field("header_image_position");

$url = get_field("url");
$member = get_field("member");

if (!$member && $url) {
    header("Location: $url");
    die();
}

$links = get_field("links");
$logo = get_field("logo");

$spoelkeuken_url = get_field("spoelkeuken_url");

$projects = get_posts([
    "post_type" => "project",
    "meta_query" => [
        [
            "key" => "organisations", // name of custom field
            "value" => '"' . $post->ID . '"',
            "compare" => "LIKE",
        ],
    ],
]);
$context["projects"] =
    count($projects) > 0 ? Timber::get_posts($projects) : null;

$org_related_posts = [];

$org_related_posts = Timber::get_posts([
    "meta_query" => [
        [
            "key" => "organisation", // The custom field key
            "value" => '"' . $post->ID . '"', // The organisation ID
            "compare" => "LIKE", // Ensures exact match
        ],
    ],
    "posts_per_page" => 4,
    //'post__not_in' => array($post->ID),
    "post_status" => "publish",
    "orderby" => ["date" => "DESC"],
]);
$context["org_related_posts"] = $org_related_posts;
$context["org_query"] = "o=" . $post->ID;

$context["header_image_position"] = $header_image_position;
$context["logo"] = $logo;
$context["links"] = $links;
$context["url"] = $url;

$context["buttons"] = [];
// if ($url) {
//     array_push($context["buttons"], [
//         "presentation" => "primary",
//         "link" => [
//             "target" => "_blank",
//             "url" => $url,
//             "title" => "Homepage",
//         ],
//     ]);
// }
if ($spoelkeuken_url) {
    if (!$context["links"]) {
        $context["links"] = [];
    }
    array_push($context["links"], [
        "link" => ["url" => $spoelkeuken_url, "title" => "Spoelkeuken"],
        "title" => __(
            'Curious about this organisation\'s tech stack?',
            "publicspaces"
        ),
        "theme" => "",
        "background_image" => get_field("spoelkeuken_backdrop", "option"),
    ]);
}

$context["spoelkeuken_url"] = $spoelkeuken_url;

$context["spoelkeuken_backdrop"] = get_field("spoelkeuken_backdrop", "option");

Timber::render("org.twig", $context);
