<?php

class MorseCodeGenerator
{
    private $code;
    private $width = 100;
    private $height = 56;

    public function __construct()
    {
        $this->code = [
            // ' ' => '',
            'a' => '.-',
            'b' => '-...',
            'c' => '-.-.',
            'd' => '-..',
            'e' => '.',
            'f' => '..-.',
            'g' => '--.',
            'h' => '....',
            'i' => '..',
            'j' => '.---',
            'k' => '-.-',
            'l' => '.-..',
            'm' => '--',
            'n' => '-.',
            'o' => '---',
            'p' => '.--.',
            'q' => '--.-',
            'r' => '.-.',
            's' => '...',
            't' => '-',
            'u' => '..-',
            'v' => '...-',
            'w' => '.--',
            'x' => '-..-',
            'y' => '-.--',
            'z' => '--..',
            '0' => '-----',
            '1' => '.----',
            '2' => '..---',
            '3' => '...--',
            '4' => '....-',
            '5' => '.....',
            '6' => '-....',
            '7' => '--...',
            '8' => '---..',
            '9' => '----.',
        ];
    }

    public function generateMorseSVG($input)
    {
        $lines = explode(" ", $input);

        $padding = 5;
        $size = count($lines) > 0 ? ($this->height - $padding * 2) / count($lines) : ($this->height - $padding * 2);

        $result = [];

        $hue = $this->stringToHue($input);
        $background = $this->hslToHex($hue, 20, 20);

        $hue = $hue - 70;
        if ($hue < 0) $hue = 360 + $hue;
        $foreground = $this->hslToHex($hue, 100, 70);

        $y = 0;

        foreach ($lines as $line) {
            $x = 0;
            $chars = str_split($line);

            foreach ($chars as $char) {

                if (!isset($this->code[strtolower($char)])) continue;
                
                $signals = str_split(strtolower($this->code[strtolower($char)]));

                foreach ($signals as $signal) {
                    $obj = [
                        'signal' => $signal,
                        'x' => $signal === '.' ? $x + ($size) / 2 + $padding : $x + $padding,
                        'y' => $signal === '.' ? $y * $size + ($size / 2) + $padding : $y * $size + $padding,
                        'w' => $size * 3,
                        'h' => $size,
                        'r' => ($size) / 2,
                    ];

                    $x += $signal === '.' ? $size : $size * 3;

                    $result[$y][] = $obj;
                }
            }

            $y += 1;
        }

        // Generate SVG string
        $svgString = '<svg width="'.$this->width.'" height="'.$this->height.'" viewBox="0 0 '.$this->width.' '.$this->height.'" fill="none" xmlns="http://www.w3.org/2000/svg">';

        $svgString .= '<rect width="'.$this->width.'" height="'.$this->height.'" fill="' . $background . '" />';
        $svgString .= '<mask id="mask">';
        $svgString .= '<rect x="' . $padding . '" y="' . $padding . '" width="' . ($this->width - ($padding)) . '" height="' . ($this->height - ($padding * 2)) . '" fill="white" />';
        $svgString .= '</mask>';
        $svgString .= '<g mask="url(#mask)">';

        foreach ($result as $lineIndex => $line) {
            foreach ($line as $objIndex => $obj) {
                if ($obj['signal'] === '.') {
                    $svgString .= '<circle id="' . $lineIndex . $objIndex . '" cx="' . $obj['x'] . '" cy="' . $obj['y'] . '" r="' . $obj['r'] . '" fill="' . $foreground . '" transform="translate(' . $obj['x'] . ', ' . $obj['y'] . ') scale(.6) translate(-' . $obj['x'] . ', -' . $obj['y'] . ')" />';
                } elseif ($obj['signal'] === '-') {
                    $svgString .= '<rect x="' . $obj['x'] . '" y="' . $obj['y'] . '" width="' . $obj['w'] . '" height="' . $obj['h'] . '" fill="' . $foreground . '" transform="translate(' . ($obj['x'] + $obj['w'] / 2) . ', ' . ($obj['y'] + $obj['h'] / 2) . ') scale(.9, .6) translate(-' . ($obj['x'] + $obj['w'] / 2) . ', -' . ($obj['y'] + $obj['h'] / 2) . ')" />';
                }
            }
        }

        $svgString .= '</g>';
        $svgString .= '</svg>';

        return $svgString;
    }

    public function stringToHue($input)
    {
        $hash = crc32($input);
        $hue = abs($hash % 360);
        return $hue;
    }

    function hslToHex($h, $s, $l)
    {
        $l /= 100;
        $a = ($s * min($l, 1 - $l)) / 100;

        $f = function ($n) use ($h, $l, $a) {
            $k = fmod(($n + $h / 30), 12);
            $color = $l - $a * max(min($k - 3, 9 - $k, 1), -1);
            return str_pad(dechex(round(255 * $color)), 2, '0', STR_PAD_LEFT);
        };

        return '#' . $f(0) . $f(8) . $f(4);
    }

    function hexToRgb($hex)
    {
        $hex = ltrim($hex, '#');
        $value = hexdec($hex);
        $r = ($value >> 16) & 255;
        $g = ($value >> 8) & 255;
        $b = $value & 255;

        return [$r, $g, $b];
    }
}
