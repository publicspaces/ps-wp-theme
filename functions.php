<?php

/**
 * Timber setup.
 */
require_once __DIR__ . "/vendor/autoload.php";
Timber\Timber::init();

Timber::$dirname = ["templates", "views"];
Timber::$autoescape = false;

class PublicSpacesSite extends Timber\Site
{
    public function __construct()
    {
        add_action("after_setup_theme", [$this, "load_theme_textdomain"]);
        add_filter("timber/context", [$this, "add_to_context"]);
        add_filter("timber/twig", [$this, "add_to_twig"]);
        add_filter("timber/locations", [$this, "twig_template_path"]);
        add_action("after_setup_theme", [$this, "theme_supports"]);
        add_action("init", [$this, "register_menus"]);
        add_action("customize_register", [$this, "customize_register"]);
        add_action("init", [$this, "register_acf_blocks"]);
        add_filter(
            "allowed_block_types_all",
            [$this, "acf_allowed_blocks"],
            10,
            2
        );
        add_filter("the_content", [$this, "example_add_img_class"]);
        add_action("publish_post", [$this, "default_featured_image"], 10, 1);

        add_filter("the_author", [$this, "guest_author_name"]);
        add_filter("get_the_author_display_name", [$this, "guest_author_name"]);
        add_filter("embed_oembed_html", [$this, "custom_oembed_filter"], 99, 4);
        add_filter(
            "woocommerce_product_add_to_cart_text",
            [$this, "change_select_options_button_text"],
            9999,
            2
        );

        add_action("wp_enqueue_scripts", [$this, "add_badge_scripts"]);
        add_action("wp_head", [$this, "activitypub_creator_meta"], 2);
        add_action("wp_head", [$this, "add_display_europe_code"], 2000);
        add_filter("get_bloginfo_rss", [$this, "modify_rss_language"], 10, 2);
        parent::__construct();
        self::includes();
    }

    public function activitypub_creator_meta()
    {
        echo '<meta name="fediverse:creator" content="@publicspaces@publicspaces.net">';
        echo '<link rel="me" href="https://social.publicspaces.net/@publicspaces">';
    }

    public function add_display_europe_code()
    {
        if (is_single() && has_tag("data-morgana")) {
            echo '<script type="module" crossorigin src="https://overlay.displayeurope.eu/init"></script>';
        }
    }

    public function twig_template_path($paths)
    {
        $paths[] = [realpath(get_template_directory() . "/../")];
        return $paths;
    }

    public function theme_supports()
    {
        add_theme_support("automatic-feed-links");
        add_theme_support("title-tag");
        add_theme_support("post-thumbnails");

        add_post_type_support("page", "excerpt");

        add_theme_support("html5", [
            "comment-form",
            "comment-list",
            "gallery",
            "caption",
        ]);

        add_theme_support("menus");
        add_theme_support("woocommerce");
    }

    public function load_theme_textdomain()
    {
        load_theme_textdomain(
            "publicspaces",
            get_template_directory() . "/languages"
        );
    }

    public function register_menus()
    {
        register_nav_menu("main-menu", __("Main", "publicspaces"));
        register_nav_menu("archive-menu", __("Archive", "publicspaces"));
        register_nav_menu(
            "footer-brand-menu",
            __("Footer Brand", "publicspaces")
        );
        register_nav_menu(
            "footer-primary-menu",
            __("Footer Primary", "publicspaces")
        );
        register_nav_menu(
            "footer-secondary-menu",
            __("Footer Secodary", "publicspaces")
        );
    }

    public function add_to_context($context)
    {
        $context["site"] = $this;
        $context["main_menu"] = Timber::get_menu("main-menu");
        $context["archive_menu"] = Timber::get_menu("archive-menu");
        $context["footer_brand_menu"] = Timber::get_menu("footer-brand-menu");
        $context["footer_primary_menu"] = Timber::get_menu(
            "footer-primary-menu"
        );
        $context["footer_secondary_menu"] = Timber::get_menu(
            "footer-secondary-menu"
        );

        $context["footer_description"] = get_field(
            "footer_description",
            "option"
        );
        $context["show_search"] = get_field("show_search", "option");

        return $context;
    }

    public function customize_register($wp_customize)
    {
        $wp_customize->add_section("theme_colors", [
            "title" => __("Theme colors", "publicspaces"),
            "priority" => 30,
        ]);

        $wp_customize->add_setting("theme_foreground_color", [
            "default" => "#000000",
            "sanitize_callback" => "sanitize_hex_color",
        ]);

        $wp_customize->add_setting("theme_background_color", [
            "default" => "#ffffff",
            "sanitize_callback" => "sanitize_hex_color",
        ]);

        $wp_customize->add_control(
            new WP_Customize_Color_Control(
                $wp_customize,
                "theme_foreground_color",
                [
                    "label" => __("Foreground color", "publicspaces"),
                    "section" => "theme_colors",
                ]
            )
        );

        $wp_customize->add_control(
            new WP_Customize_Color_Control(
                $wp_customize,
                "theme_background_color",
                [
                    "label" => __("Background color", "publicspaces"),
                    "section" => "theme_colors",
                ]
            )
        );

        return $wp_customize;
    }

    function add_to_twig($twig)
    {
        $twig->addFunction(
            new \Twig\TwigFunction("get_theme_colors", [
                $this,
                "get_theme_colors",
            ])
        );
        $twig->addFunction(
            new \Twig\TwigFunction("get_theme_foreground_color", [
                $this,
                "get_theme_foreground_color",
            ])
        );
        $twig->addFunction(
            new \Twig\TwigFunction("get_theme_background_color", [
                $this,
                "get_theme_background_color",
            ])
        );
        $twig->addFunction(
            new \Twig\TwigFunction("get_block_colors", [
                $this,
                "get_block_colors",
            ])
        );
        $twig->addFunction(
            new \Twig\TwigFunction("is_editing", [$this, "is_editing"])
        );
        $twig->addFunction(
            new \Twig\TwigFunction("get_translations", [
                $this,
                "get_translations",
            ])
        );
        return $twig;
    }

    public function add_badge_scripts()
    {
        wp_enqueue_script(
            "publicbadges-module",
            "https://assets.publicspaces.net/@publicbadges/badge@latest/dist/publicbadges/publicbadges.esm.js",
            [],
            null,
            true
        );

        // Enqueue the nomodule script
        wp_enqueue_script(
            "publicbadges-nomodule",
            "https://assets.publicspaces.net/@publicbadges/badge@latest/dist/publicbadges/publicbadges.js",
            [],
            null,
            true
        );

        // Add the nomodule attribute to the non-module script
        add_filter(
            "script_loader_tag",
            function ($tag, $handle, $src) {
                if ($handle === "publicbadges-nomodule") {
                    $tag =
                        '<script nomodule src="' .
                        esc_url($src) .
                        '"></script>';
                }
                if ($handle === "publicbadges-module") {
                    $tag =
                        '<script type="module" src="' .
                        esc_url($src) .
                        '"></script>';
                }
                return $tag;
            },
            10,
            3
        );
    }

    public function get_theme_colors(
        $forefround_color = "",
        $background_color = ""
    ) {
        return "--foreground: " .
            $this->get_theme_foreground_color() .
            "; --background: " .
            $this->get_theme_background_color() .
            ";";
    }

    public function get_theme_foreground_color()
    {
        $theme_foreground_color = get_theme_mod(
            "theme_foreground_color",
            "#000000"
        );

        if ((is_single() || is_page()) && get_field("theme_override")) {
            $theme_foreground_color = get_field("theme_foreground_color");
        }

        return $theme_foreground_color;
    }
    public function get_theme_background_color()
    {
        $theme_background_color = get_theme_mod(
            "theme_background_color",
            "#ffffff"
        );

        if ((is_single() || is_page()) && get_field("theme_override")) {
            $theme_background_color = get_field("theme_background_color");
        }

        return $theme_background_color;
    }

    public function get_block_colors(
        $forefround_color = "",
        $background_color = ""
    ) {
        return "--foreground: " .
            $forefround_color .
            "; --background: " .
            $background_color .
            ";";
    }

    public function is_editing()
    {
        if (!function_exists("get_current_screen")) {
            return false;
        }
        return get_current_screen()->is_block_editor ?: true;
    }

    public function get_translations()
    {
        if (!is_plugin_active("multilingualpress/multilingualpress.php")) {
            return [];
        }
        $t = \Inpsyde\MultilingualPress\translationIds(
            get_the_ID(),
            "post",
            get_current_blog_id()
        );
        if (count($t) < 2) {
            return [];
        }

        $args = \Inpsyde\MultilingualPress\Framework\Api\TranslationSearchArgs::forContext(
            new \Inpsyde\MultilingualPress\Framework\WordpressContext()
        )
            ->forSiteId(get_current_blog_id())
            ->includeBase();

        $translations = \Inpsyde\MultilingualPress\resolve(
            \Inpsyde\MultilingualPress\Framework\Api\Translations::class
        )->searchTranslations($args);

        return $translations;
    }

    static function includes()
    {
        // require_once dirname(__FILE__) . '/includes/string-translations.php';
        // require_once dirname(__FILE__) . '/includes/gallery.php';
    }

    public function register_acf_blocks()
    {
        register_block_type(__DIR__ . "/blocks/section");
        register_block_type(__DIR__ . "/blocks/grid");
        register_block_type(__DIR__ . "/blocks/jumbo");
        register_block_type(__DIR__ . "/blocks/call-to-action");
        register_block_type(__DIR__ . "/blocks/card");
        register_block_type(__DIR__ . "/blocks/query");
    }

    public function acf_allowed_blocks($allowed_blocks, $post)
    {
        if ($post->post->post_type === "page") {
            $core_blocks = WP_Block_Type_Registry::get_instance()->get_all_registered();
            // only allow custom blocks on pages
            // $core_blocks = array();
            $custom_blocks = ["acf/jumbo"];
        } else {
            $core_blocks = WP_Block_Type_Registry::get_instance()->get_all_registered();
            $custom_blocks = [];
        }

        $allowed_blocks = array_merge($core_blocks, $custom_blocks);
        return $allowed_blocks;
    }

    /**
     * Remove the wp-block-image class from figures so figcations are styled correctly
     */
    public function example_add_img_class($content)
    {
        // $pattern        = "/<figure(.*?)(class.*?)>/i";
        // $replacement    = '<figure>';
        // $content        = preg_replace($pattern, $replacement, $content);
        return $content;
    }

    /**
     * Add a generated morse code image if the featured image does not exist
     */
    public function default_featured_image($post_id)
    {
        require_once ABSPATH . "wp-admin/includes/media.php";
        require_once ABSPATH . "wp-admin/includes/file.php";
        require_once ABSPATH . "wp-admin/includes/image.php";
        require_once __DIR__ . "/includes/morse-svg.php";

        $current_post_thumbnail = get_the_post_thumbnail_url($post_id);

        // If there's already a featured image, do nothing
        if ($current_post_thumbnail) {
            return;
        }

        $morseGenerator = new MorseCodeGenerator();
        $title = get_the_title($post_id);
        $svg = $morseGenerator->generateMorseSVG($title);

        $base64_img = base64_encode($svg);
        $upload_dir = wp_upload_dir();
        $upload_path =
            str_replace("/", DIRECTORY_SEPARATOR, $upload_dir["path"]) .
            DIRECTORY_SEPARATOR;

        $img = str_replace("data:image/jpeg;base64,", "", $base64_img);
        $img = str_replace(" ", "+", $img);
        $decoded = base64_decode($img);
        $filename = "featured_image_" . $post_id . ".svg";
        $file_type = "image/svg+xml";
        $hashed_filename = md5($filename . microtime()) . "_" . $filename;

        // Save the image in the uploads directory.
        $upload_file = file_put_contents(
            $upload_path . $hashed_filename,
            $decoded
        );

        $attachment = [
            "post_mime_type" => $file_type,
            "post_title" => preg_replace(
                '/\.[^.]+$/',
                "",
                basename($hashed_filename)
            ),
            "post_content" => "",
            "post_status" => "inherit",
            "guid" => $upload_dir["url"] . "/" . basename($hashed_filename),
        ];

        $attach_id = wp_insert_attachment(
            $attachment,
            $upload_dir["path"] . "/" . $hashed_filename
        );
        set_post_thumbnail($post_id, $attach_id);
    }

    /**
     * Modify oEmbedded iframes to be full width, but keep aspect ratio
     */
    public function custom_oembed_filter($html, $url, $attr, $post_ID)
    {
        // Check if the embed is an iframe
        if (strpos($html, "<iframe") !== false) {
            // Use preg_match to find the current width and height
            preg_match('/width="([0-9]+)"/', $html, $widthMatches);
            preg_match('/height="([0-9]+)"/', $html, $heightMatches);

            if (!empty($widthMatches[1]) && !empty($heightMatches[1])) {
                // Calculate the aspect ratio
                $aspectRatio = $heightMatches[1] / $widthMatches[1];
                // Set width to 100% and calculate new height based on the aspect ratio
                $newHeight = 100 * $aspectRatio; // This will be a percentage relative to the width

                // Replace the width and height in the iframe with new values
                $html = str_replace($widthMatches[0], 'width="100%"', $html);
                $html = str_replace(
                    $heightMatches[0],
                    'height="' . $newHeight . '%"',
                    $html
                );

                // Add inline styles to maintain aspect ratio
                $html =
                    '<div style="position: relative; width: 100%; height: 0; padding-bottom: ' .
                    $newHeight .
                    '%;">' .
                    $html .
                    "</div>";
                $html = str_replace(
                    "<iframe",
                    '<iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"',
                    $html
                );
            }
        }

        return $html;
    }

    public function change_select_options_button_text($label, $product)
    {
        //if ($product->is_type("variable")) {
        return "Bekijk details";
        //}
        //return $label;
    }

    /**
     * Add support for Guest Author field on posts
     */
    public function guest_author_name($name)
    {
        global $post;
        $author = get_post_meta($post->ID, "guest_author", true);
        if ($author) {
            $name = $author;
        }
        return $name;
    }

    public function modify_rss_language($language, $show)
    {
        if ($show === "language") {
            return substr($language, 0, 2); // Extract only the first two letters (e.g., 'nl' from 'nl_NL')
        }
        return $language;
    }
}

new PublicSpacesSite();

/**
 * Theme setup.
 */
function tailpress_setup()
{
    add_theme_support("title-tag");

    // register_nav_menus(
    // 	array(
    // 		'primary' => __( 'Primary Menu', 'tailpress' ),
    // 	)
    // );

    add_theme_support("html5", [
        "search-form",
        "comment-form",
        "comment-list",
        "gallery",
        "caption",
    ]);

    add_theme_support("custom-logo");
    add_theme_support("post-thumbnails");

    add_theme_support("align-wide");
    add_theme_support("wp-block-styles");

    add_theme_support("editor-styles");
    add_editor_style("css/editor-style.css");
}

add_action("after_setup_theme", "tailpress_setup");

/**
 * Enqueue theme assets.
 */
function tailpress_enqueue_scripts()
{
    $theme = wp_get_theme();

    wp_enqueue_style(
        "publicspaces",
        tailpress_asset("css/publicspaces.css"),
        [],
        $theme->get("Version")
    );
    wp_enqueue_style(
        "tailpress",
        tailpress_asset("css/app.css"),
        [],
        $theme->get("Version")
    );

    wp_enqueue_script(
        "tailpress",
        tailpress_asset("js/app.js"),
        [],
        $theme->get("Version")
    );
}

add_action("wp_enqueue_scripts", "tailpress_enqueue_scripts");

/**
 * Get asset path.
 *
 * @param string  $path Path to asset.
 *
 * @return string
 */
function tailpress_asset($path)
{
    if (wp_get_environment_type() === "production") {
        return get_template_directory_uri() . "/" . $path;
    }

    return add_query_arg(
        "time",
        time(),
        get_template_directory_uri() . "/" . $path
    );
}

/**
 * Adds option 'li_class' to 'wp_nav_menu'.
 *
 * @param string  $classes String of classes.
 * @param mixed   $item The current item.
 * @param WP_Term $args Holds the nav menu arguments.
 *
 * @return array
 */
function tailpress_nav_menu_add_li_class($classes, $item, $args, $depth)
{
    if (isset($args->li_class)) {
        $classes[] = $args->li_class;
    }

    if (isset($args->{"li_class_$depth"})) {
        $classes[] = $args->{"li_class_$depth"};
    }

    return $classes;
}

add_filter("nav_menu_css_class", "tailpress_nav_menu_add_li_class", 10, 4);

/**
 * Adds option 'submenu_class' to 'wp_nav_menu'.
 *
 * @param string  $classes String of classes.
 * @param mixed   $item The current item.
 * @param WP_Term $args Holds the nav menu arguments.
 *
 * @return array
 */
function tailpress_nav_menu_add_submenu_class($classes, $args, $depth)
{
    if (isset($args->submenu_class)) {
        $classes[] = $args->submenu_class;
    }

    if (isset($args->{"submenu_class_$depth"})) {
        $classes[] = $args->{"submenu_class_$depth"};
    }

    return $classes;
}

add_filter(
    "nav_menu_submenu_css_class",
    "tailpress_nav_menu_add_submenu_class",
    10,
    3
);

/**
 *
 * Description: Show related posts by # of matching tags
 * Author: Fayaz Ahmed
 * Author URI: https://fayaz.dev/
 * Found at: https://wordpress.stackexchange.com/questions/413632/get-related-posts-matching-most-of-the-provided-tags-using-wp-query
 *
 * Any WP_Query where 'order_by_most_tags' is set as an argument,
 * will be altered by this function, so that the query result is
 * ordered by the number of matching tags in descending order.
 * However, if no matching tag is provided, then that query will not
 * be altered by this.
 */
function filter_posts_by_most_tags($query)
{
    if (
        isset($query->query["order_by_most_tags"]) &&
        isset($query->query_vars["tag_slug__in"]) &&
        !empty($query->query_vars["tag_slug__in"])
    ) {
        add_filter("posts_clauses", "add_select_count_tags", 10, 2);
    }
}
add_action("pre_get_posts", "filter_posts_by_most_tags");

function add_select_count_tags($clauses, $wp_query)
{
    global $wpdb;

    // database query modification needed for ordering based on most tags
    $clauses["fields"] =
        $clauses["fields"] .
        ", count( " .
        $wpdb->term_relationships .
        ".object_id ) as number_of_tags";
    $clauses["orderby"] = "number_of_tags DESC, " . $clauses["orderby"];

    // we only need this once, so it's better to remove the filter after use
    remove_filter("posts_clauses", "add_select_count_tags", 10, 2);
    return $clauses;
}

function timber_set_product($post)
{
    global $product;

    if (is_woocommerce()) {
        $product = wc_get_product($post->ID);
    }
}
